from django.urls import path

from . import views

app_name = "page" 

urlpatterns = [
    path('', views.home, name='home'),
    path('aboutme/', views.aboutme, name='aboutme'),
    path('experiences/', views.experience, name='experience'),
    path('project/', views.project, name='project'),
    path('gallery/', views.gallery, name='gallery'),
    path('contact/', views.contact, name='contact'),
]
