from django.shortcuts import render

def home(request):
    return render(request, 'page/HOME.html')

def aboutme(request):
    return render(request, 'page/ABOUT.html')

def experience(request):
    return render(request, 'page/EXPERIENCES.html')

def project(request):
    return render(request, 'page/PROJECT.html')

def gallery(request):
    return render(request, 'page/GALLERY.html')

def contact(request):
    return render(request, 'page/CONTACT.html')
